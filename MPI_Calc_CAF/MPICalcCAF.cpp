// MPICalcCAF.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <stdio.h>
#include <mpi.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <chrono>
#include "CAF.h"
#include "cu_func.h"

int main(int argc, char* argv[])
{
	/** Создание объекта CAF.*/
	CAF caf(800, 1600, 240, 25, 9600, 9600, 48, 10, 0, 10);
	/** ВВЗ.*/
	int time_delay = caf.Get_Delay();
	
	caf.CreateBitSignals(time_delay);
	caf.SelectModulation(false);

	/** Размер вектора исследуемого сигнала.*/
	size_t size_vec_explr_sig = caf.Vec_ExplrSigEx.size();

	/** Размер вектора опорного сигнала.*/
	size_t size_vec_ref_sig = caf.Vec_RefSigEx.size();

	/** Размер вектора значений ВФН.*/
	size_t size_CAF = size_vec_explr_sig - size_vec_ref_sig;

	/** Шаг децимации.*/
	/** Для использования шага децимации K = 3, необходимо домножить на 3
	* число отсчетов сигналов в функциях BPSK_Modulation и MSK_Modulation.*/
	int K = 4;

	/** Выделение памяти для вектора ВКФ.*/
	caf.vec_amb_func.resize(size_CAF);
	for (size_t i = 0; i < size_CAF; i++)
	{
		caf.vec_amb_func[i].resize(size_vec_ref_sig / K);
	}

	/** Максимум из каждого вектора vec_amb_func.*/
	float maximum;
	/** Вектор максимумов ВФН.*/
	std::vector<float> Vec_CAF_Buf;
	Vec_CAF_Buf.resize(size_CAF);

	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	if (!prop.canMapHostMemory)
		exit(0);
	cudaSetDeviceFlags(cudaDeviceMapHost);

	/** Размер блока для вычисления произведения отсчетов сигналов.*/
	const unsigned int blockSizeSigMult = 512;
	/** Число блоков для вычисления произведения отсчетов сигналов.*/
	const unsigned int numBlocksSigMult = (unsigned int)size_vec_ref_sig / blockSizeSigMult;
	/** Количество нитей для вычисления произведения отсчетов сигналов.*/
	const unsigned int numItemsSigMult = blockSizeSigMult * numBlocksSigMult;

	/** Выделение памяти на GPU для опорного и исследуемого сигналов.*/
	cufftComplex *pHostRefSig, *pDevRefSig;
	cudaHostAlloc((void**)&pHostRefSig, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevRefSig, (void*)pHostRefSig, 0);
	for (int it = 0; it < size_vec_ref_sig; it++)
	{
		pHostRefSig[it].x = caf.Vec_RefSigEx[it].x;
		pHostRefSig[it].y = caf.Vec_RefSigEx[it].y;
	}
	cufftComplex *pHostExplrSig, *pDevExplrSig;
	cudaHostAlloc((void**)&pHostExplrSig, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevExplrSig, (void*)pHostExplrSig, 0);
	cufftComplex *pHostRes, *pDevRes;
	cudaHostAlloc((void**)&pHostRes, sizeof(cufftComplex) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevRes, (void*)pHostRes, 0);
	/** Выделение памяти на GPU для вычисления БПФ.*/
	cufftComplex *host_arr_decim, *dev_arr_decim;
	cudaHostAlloc((void**)&host_arr_decim, sizeof(cufftComplex) * size_vec_ref_sig / K, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&dev_arr_decim, (void*)host_arr_decim, 0);
	/** Создание конфигурации для БПФ.*/
	cufftHandle plan;
	if (cufftPlan1d(&plan, (int)size_vec_ref_sig / K, CUFFT_C2C, BATCH) != CUFFT_SUCCESS)
	{
		std::cout << "CUFFT error: Plan creation failed" << std::endl;
	}

	/** Размер блока для вычисления модуля БПФ после перемножения отсчетов сигналов.*/
	const unsigned int blockSizeAbsFFT = 512;
	/** Число блоков для вычисления модуля БПФ после перемножения отсчетов сигналов.*/
	const unsigned int numBlocksAbsFFT = (unsigned int)size_vec_ref_sig / K / blockSizeSigMult;
	/** Количество нитей для вычисления модуля БПФ после перемножения отсчетов сигналов.*/
	const unsigned int numItemsAbsFFT = blockSizeAbsFFT * numBlocksAbsFFT;

	/** Выделение памяти на GPU для вычисления модуля ВФН.*/
	float *pDevResAbsFFT, *pHostResAbsFFT;
	cudaHostAlloc((void**)&pHostResAbsFFT, sizeof(float) * size_vec_ref_sig, cudaHostAllocMapped);
	cudaHostGetDevicePointer((void**)&pDevResAbsFFT, (void*)pHostResAbsFFT, 0);
	/** Вычисление ФН.*/
	for (size_t i = 0; i < size_CAF; i++)
	{
		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			pHostExplrSig[j].x = caf.Vec_ExplrSigEx[j + i].x;
			pHostExplrSig[j].y = caf.Vec_ExplrSigEx[j + i].y;
		}
	
		signal_mult(numBlocksSigMult, blockSizeSigMult, pDevRefSig, pDevExplrSig, pDevRes);
		
		cudaThreadSynchronize();

		int k;
		int it = 0;
		size_t j = 0;
		memset(host_arr_decim, 0, sizeof(cufftComplex) * size_vec_ref_sig / K);
		while(j < size_vec_ref_sig)
		{
			k = 0;
			while (k < K)
			{
				host_arr_decim[it].x += pHostRes[j].x;
				host_arr_decim[it].y += pHostRes[j].y;
				k++; j++;
			}
			it++;
		}

		cufftExecC2C(plan, dev_arr_decim, dev_arr_decim, CUFFT_FORWARD);
		cudaThreadSynchronize();

		abs_fft(numBlocksAbsFFT, blockSizeAbsFFT, dev_arr_decim, pDevResAbsFFT);

		cudaThreadSynchronize();
		
		for (int it = 0; it < size_vec_ref_sig / K; it++)
		{
			caf.vec_amb_func[i][it] = pHostResAbsFFT[it];
		}
		
		maximum = 0;
		for (size_t k = 0; k < caf.vec_amb_func[i].size(); k++)
		{
			if (maximum < caf.vec_amb_func[i][k])
				maximum = caf.vec_amb_func[i][k];
		}
		Vec_CAF_Buf[i] = maximum;
	}

	cudaFreeHost(pHostRefSig);
	cudaFreeHost(pHostExplrSig);
	cudaFreeHost(pHostRes);

	cudaFreeHost(host_arr_decim);
	cufftDestroy(plan);

	cudaFreeHost(pHostResAbsFFT);

	int max_idx;
	float max = 0;
	for (size_t it = 0; it < size_CAF; it++)
	{
		if (max < Vec_CAF_Buf[it])
		{
			max = Vec_CAF_Buf[it];
			max_idx = int(it);
		}
	}

	std::cout << "Maximum: " << max << std::endl;
	std::cout << "Delay: " << max_idx << std::endl;

	return 0;
}