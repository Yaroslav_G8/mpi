#include"cu_func.h"
#include<vector>
#include<iostream>

/** ����, ����������� ������������ ������� ������� �������� � ������������ ��������.*/
__global__ void kernel_signal_mult(cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	pDevRes[index].x = pDevRefSig[index].x * pDevExplrSig[index].x + pDevRefSig[index].y * pDevExplrSig[index].y;
	pDevRes[index].y = pDevRefSig[index].x * (-pDevExplrSig[index].y) + pDevRefSig[index].y * pDevExplrSig[index].x;
}

/** �������, � ������� ����������� �� ���������� ���� kernel_signal_mult.*/
void signal_mult(const unsigned int & numBlocks, const unsigned int & blockSize,
	cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes)
{
	kernel_signal_mult << <numBlocks, blockSize >> > (pDevRefSig, pDevExplrSig, pDevRes);
}

/** ����, ����������� ������ ��� ����� ��������� �������� ��������.*/
__global__ void kernel_abs_fft(cufftComplex * pDevSigDec, float * pDevAbsFFT)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	pDevAbsFFT[index] = sqrt(pDevSigDec[index].x * pDevSigDec[index].x + pDevSigDec[index].y * pDevSigDec[index].y);
}

void abs_fft(const unsigned int & numBlocks, const unsigned int & blockSize,
	cufftComplex * pDevSigDec, float * pDevAbsFFT)
{
	kernel_abs_fft << <numBlocks, blockSize >> > (pDevSigDec, pDevAbsFFT);
}