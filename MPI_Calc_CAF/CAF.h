#pragma once

#include<cuda.h>
#include<cuda_runtime_api.h>
#include<cufft.h>
#include<vector>

struct cmplx
{
	float re;
	float im;
};

class CAF
{
private:
	/** ���������� ��� � ������� �������.*/
	int _N1;
	/** ���������� ��� � ����������� �������.*/
	int _N2;
	/** ��������� �������� �������� �������.*/
	int _time_delay;
	/** ������� ������� �������� �/��� ������������ �������.*/
	float _fo;
	/** �������� ��������.*/
	int _Br;
	/** ������ ���������.*/
	float _T;
	/** ������� ������������� �������� �/��� ������������ �������.*/
	float _Fs;
	/** ������� ������� ��� ������������ �������.*/
	float _dopler_freq;
	// ������� ������� ��� �������� �������
	float _dopler_freq_for_ref_sig;
	/** ��������� ������/���(��).*/
	float _d;
public:

	/** ����������� �� ���������.*/
	CAF();

	/** ����������� � �����������.*/
	CAF(
	int _N1, int _N2, int _time_delay,
		float _fo, int _Br, float _T, float _Fs,
		float _dopler_freq, float _dopler_freq_for_ref_sig, float _d);

	/** �������� �������� ��������� ��������.*/
	int Get_Delay();

	/** ������������ ������� ������.*/
	std::vector <bool> vec_refsig;
	/** ������������ ����������� ������.*/
	std::vector <bool> vec_explrsig;
	/** ������ �������� �������� �������.*/
	std::vector<cufftComplex> Vec_RefSig;
	/** ������ �������� �������� ������� (����������� �� 2^n).*/
	std::vector<cufftComplex> Vec_RefSigEx;
	/** ������ �������� ������������ �������.*/
	std::vector<cufftComplex> Vec_ExplrSig;
	/** ������ �������� ������������ ������� (����������� �� 2^n).*/
	std::vector<cufftComplex> Vec_ExplrSigEx;
	/** ������ �������� ��.*/
	std::vector<std::vector<float>> vec_amb_func;
	/** ������ �������� �� (�������� ������ ���������).*/
	std::vector<float> Vec_CAF;

	/** ����� ��������� ������ ����� � x, ������ ������� ������.
	* @param x - ��������, ������� ���������� ��������� �� ������� ������.
	*/
	int pow_2(int x);

	/** ������������� ������������ �������(������� � �����������).
	* ������� ������������ ����� ������������������ ���(0 � 1).
	* @param delay - ��������� �������� �������� �������.
	*/
	void CreateBitSignals(int delay);

	/** �������� ��� �� �������.
	* @param &Vec_Buf - ������, �� ������� ����������� ���.
	*/
	void Noise(std::vector <float> & Vec_Buf);

	/** �������� ��������� BPSK.*/
	void ModulationBPSK();

	/** �������� ��������� MSK.*/
	void ModulationMSK();

	/** ������� ��� ���������.
	* @param BPSK_or_MSK - ���� BPSK_or_MSK = true, �� BPSK - ���������, ����� MSK - ���������
	*/
	void SelectModulation(bool BPSK_or_MSK);

	/** ���������� ���.
	* @param *data - ������ ��� ������� ���������� ���.
	* @param n - ���������� �������� � �������.
	* @param is - ���� is = -1 - ������, ���� is = 1 - ��������.
	*/
	void fourier(cufftComplex *data, int n, int is);

	/** ��������� �������� ������� ����������������.*/
	//void calc_caf();

	~CAF();
};

