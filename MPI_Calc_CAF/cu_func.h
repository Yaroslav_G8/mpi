#pragma once

#include<cuda.h>
#include<cuda_runtime_api.h>
#include<cufft.h>
#include<vector>

#define BATCH 1

/** ����, ����������� ������������ ������� ������� �������� � ������������ ��������.*/
__global__ void kernel_signal_mult(cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes);
/** �������, � ������� ����������� �� ���������� ���� kernel_signal_mult.*/
void signal_mult(const unsigned int & numBlocks, const unsigned int & blockSize,
	cufftComplex * pDevRefSig, cufftComplex * pDevExplrSig, cufftComplex * pDevRes);

/** ����, ����������� ������ ��� ����� ��������� �������� ��������.*/
__global__ void kernel_abs_fft(cufftComplex * pDevSigDec, float * pDevAbsFFT);
/** �������, � ������� ����������� �� ���������� ���� kernel_abs_fft.*/
void abs_fft(const unsigned int & numBlocks, const unsigned int & blockSize,
	cufftComplex * pDevSigDec, float * pDevAbsFFT);
