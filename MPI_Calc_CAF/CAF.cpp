#include "stdafx.h"
#include "CAF.h"
#include <algorithm>
#include"time.h"
#define _USE_MATH_DEFINES
#include"math.h"

/** ����������� �� ���������.*/
CAF::CAF()
{
	_N1 = 800;
	_N2 = 1600;
	_time_delay = 500;
	_fo = 25;
	_Br = 9600;
	_T = 1. / _Br;
	_Fs = 48;
	_dopler_freq = 10;
	_dopler_freq_for_ref_sig = 0;
	_d = 10;
}

/** ����������� � �����������.*/
CAF::CAF(
	int _N1, int _N2, int _time_delay,
	float _fo, int _Br, float _T, float _Fs,
	float _dopler_freq, float _dopler_freq_for_ref_sig, float _d)
{
	this->_N1 = _N1;
	this->_N2 = _N2;
	this->_time_delay = _time_delay;
	this->_fo = _fo;
	this->_Br = _Br;
	this->_T = 1. / _Br;
	this->_Fs = _Fs;
	this->_dopler_freq = _dopler_freq;
	this->_dopler_freq_for_ref_sig = _dopler_freq_for_ref_sig;
	this->_d = _d;
}

/** �������� �������� ��������� ��������.*/
int CAF::Get_Delay()
{
	return _time_delay;
}

/** ����� ��������� ������ ����� � x, ������ ������� ������.
* @param x - ��������, ������� ���������� ��������� �� ������� ������.
*/
int CAF::pow_2(int x)
{
	x--;
	for (int p = 1; p < 32; p <<= 1)
		x |= (x >> p);
	return ++x;
}

/** ������������� ������������ �������(������� � �����������).
* ������� ������������ ����� ������������������ ���(0 � 1).
* @param delay - ��������� �������� �������� �������.
*/
void CAF::CreateBitSignals(int delay)
{
	srand(clock());
	vec_refsig.clear();
	vec_refsig.resize(_N1);
	/** ������� ������.*/
	for (int i = 0; i < _N1; i++)
	{
		vec_refsig[i] = rand() % 2;
	}

	vec_explrsig.clear();
	vec_explrsig.resize(_N2);
	/** ����������� ������.*/
	for (int i = 0; i < _N2; i++)
	{
		vec_explrsig[i] = rand() % 2;
	}
	std::copy(vec_refsig.begin(), vec_refsig.end(), vec_explrsig.begin() + delay);
}

/** �������� ��� �� �������.
* @param &Vec_Buf - ������, �� ������� ����������� ���.
*/
void CAF::Noise(std::vector <float> & Vec_Buf)
{
	size_t size = Vec_Buf.size();
	// ��������� ����
	float sum_kv_n = 0;
	int M = 0;
	float *arr_n = new float[size];
	for (size_t i = 0; i < size; i++)
	{
		M = rand() % 9 + 12;
		float sum_Qsi = 0;
		for (int j = 0; j < M; j++)
		{
			sum_Qsi += (rand() % 21 - 10) / 10.;
		}
		arr_n[i] = sum_Qsi / M;
		sum_kv_n += arr_n[i] * arr_n[i];
	};

	float sum_kv_Sig = 0;
	for (size_t t = 0; t < size; t++)
	{
		sum_kv_Sig += Vec_Buf[t] * Vec_Buf[t];
	};

	float _alpha = 0;
	_alpha = sqrt(pow(10, -_d / 10) * sum_kv_Sig / (sum_kv_n));

	// ��������� ������� ���������� � �����
	for (size_t i = 0; i < size; i++)
	{
		Vec_Buf[i] = Vec_Buf[i] + _alpha * arr_n[i];
	};
}

/** �������� ��������� BPSK.*/
void CAF::ModulationBPSK()
{
	/** ���������� ����� � ����� ����.*/
	int amnt_pnt_in_bit = (int)(_T * _Fs * 1000);

	Vec_RefSig.clear();
	Vec_RefSig.resize(_N1 * amnt_pnt_in_bit);

	/** �������� �������� ��������
	* � �������� � ������ ������ ��� ��������� ����.*/
	std::vector<float> Vec_Buf_for_NoiseRe, Vec_Buf_for_NoiseIm;
	Vec_Buf_for_NoiseRe.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);

	Vec_Buf_for_NoiseIm.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	/** ����.*/
	float phase = 0;
	/** ������� ������ � ���������� ��2.*/
	for (int i = 0; i < _N1; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq_for_ref_sig * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			Vec_RefSig[amnt_pnt_in_bit * i + j].x = (cos(vec_refsig[i] * M_PI) *
				cos(phase) - sin(vec_refsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			Vec_RefSig[amnt_pnt_in_bit * i + j].y = (cos(vec_refsig[i] * M_PI) *
				sin(phase) + sin(vec_refsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = Vec_RefSig[amnt_pnt_in_bit * i + j].x;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = Vec_RefSig[amnt_pnt_in_bit * i + j].y;
		}
	}

	Noise(Vec_Buf_for_NoiseRe);
	Noise(Vec_Buf_for_NoiseIm);

	/** ������������ ������� �������� ������� �� ������� ������.*/
	int num_of_sam = pow_2(_N1 * amnt_pnt_in_bit);
	Vec_RefSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N1 * amnt_pnt_in_bit))
		{
			Vec_RefSigEx[i].x = Vec_RefSig[i].x;
			Vec_RefSigEx[i].y = Vec_RefSig[i].y;
		}
		else
		{
			Vec_RefSigEx[i].x = 0;
			Vec_RefSigEx[i].y = 0;
		}
	}

	/** ��������� ������� �������� ��� ������������ �������.*/
	Vec_Buf_for_NoiseRe.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);
	Vec_Buf_for_NoiseIm.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	Vec_ExplrSig.resize(_N2 * amnt_pnt_in_bit);
	phase = 0;
	// ����������� ������ � ���������� ��2
	for (int i = 0; i < _N2; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			Vec_ExplrSig[amnt_pnt_in_bit * i + j].x = (cos(vec_explrsig[i] * M_PI) *
				cos(phase) - sin(vec_explrsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			Vec_ExplrSig[amnt_pnt_in_bit * i + j].y = (cos(vec_explrsig[i] * M_PI) *
				sin(phase) + sin(vec_explrsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = Vec_ExplrSig[amnt_pnt_in_bit * i + j].x;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = Vec_ExplrSig[amnt_pnt_in_bit * i + j].y;
		}
	}

	Noise(Vec_Buf_for_NoiseRe);
	Noise(Vec_Buf_for_NoiseIm);

	/** ������������ ������� ������������ ������� �� ������� ������.*/
	num_of_sam = pow_2(_N2 * amnt_pnt_in_bit);
	Vec_ExplrSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N2 * amnt_pnt_in_bit))
		{
			Vec_ExplrSigEx[i].x = Vec_ExplrSig[i].x;
			Vec_ExplrSigEx[i].y = Vec_ExplrSig[i].y;
		}
		else
		{
			Vec_ExplrSigEx[i].x = 0;
			Vec_ExplrSigEx[i].y = 0;
		}
	}

	/** ������� �������� ��������.*/
	Vec_Buf_for_NoiseRe.clear();
	Vec_Buf_for_NoiseIm.clear();
}

/** �������� ��������� MSK.*/
void CAF::ModulationMSK()
{
	/** ���������� ����� � ����� ����.*/
	int amnt_pnt_in_bit = (int)(_T * _Fs * 1000);

	Vec_RefSig.clear();
	Vec_RefSig.resize(_N1 * amnt_pnt_in_bit);

	/** �������� �������� ��������
	* � �������� � ������ ������ ��� ��������� ����.*/
	std::vector<float> Vec_Buf_for_NoiseRe, Vec_Buf_for_NoiseIm;
	Vec_Buf_for_NoiseRe.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);

	Vec_Buf_for_NoiseIm.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	/** ����.*/
	float phase = 0;
	/** ������� ������ � ���������� ��2.*/
	for (int i = 0; i < _N1; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq_for_ref_sig * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			Vec_RefSig[amnt_pnt_in_bit * i + j].x = (cos(vec_refsig[i] * M_PI) *
				cos(phase) - sin(vec_refsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			Vec_RefSig[amnt_pnt_in_bit * i + j].y = (cos(vec_refsig[i] * M_PI) *
				sin(phase) + sin(vec_refsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = Vec_RefSig[amnt_pnt_in_bit * i + j].x;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = Vec_RefSig[amnt_pnt_in_bit * i + j].y;
		}
	}

	Noise(Vec_Buf_for_NoiseRe);
	Noise(Vec_Buf_for_NoiseIm);

	/** ������������ ������� �������� ������� �� ������� ������.*/
	int num_of_sam = pow_2(_N1 * amnt_pnt_in_bit);
	Vec_RefSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N1 * amnt_pnt_in_bit))
		{
			Vec_RefSigEx[i].x = Vec_RefSig[i].x;
			Vec_RefSigEx[i].y = Vec_RefSig[i].y;
		}
		else
		{
			Vec_RefSigEx[i].x = 0;
			Vec_RefSigEx[i].y = 0;
		}
	}

	/** ��������� ������� �������� ��� ������������ �������.*/
	Vec_Buf_for_NoiseRe.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);
	Vec_Buf_for_NoiseIm.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	Vec_ExplrSig.resize(_N2 * amnt_pnt_in_bit);
	phase = 0;
	// ����������� ������ � ���������� ��2
	for (int i = 0; i < _N2; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			Vec_ExplrSig[amnt_pnt_in_bit * i + j].x = (cos(vec_explrsig[i] * M_PI) *
				cos(phase) - sin(vec_explrsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			Vec_ExplrSig[amnt_pnt_in_bit * i + j].y = (cos(vec_explrsig[i] * M_PI) *
				sin(phase) + sin(vec_explrsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = Vec_ExplrSig[amnt_pnt_in_bit * i + j].x;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = Vec_ExplrSig[amnt_pnt_in_bit * i + j].y;
		}
	}

	Noise(Vec_Buf_for_NoiseRe);
	Noise(Vec_Buf_for_NoiseIm);

	/** ������������ ������� ������������ ������� �� ������� ������.*/
	num_of_sam = pow_2(_N2 * amnt_pnt_in_bit);
	Vec_ExplrSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N2 * amnt_pnt_in_bit))
		{
			Vec_ExplrSigEx[i].x = Vec_ExplrSig[i].x;
			Vec_ExplrSigEx[i].y = Vec_ExplrSig[i].y;
		}
		else
		{
			Vec_ExplrSigEx[i].x = 0;
			Vec_ExplrSigEx[i].y = 0;
		}
	}

	/** ������� �������� ��������.*/
	Vec_Buf_for_NoiseRe.clear();
	Vec_Buf_for_NoiseIm.clear();
}

/** ������� ��� ���������.
* @param BPSK_or_MSK - ���� BPSK_or_MSK = true, �� BPSK - ���������, ����� MSK - ���������
*/
void CAF::SelectModulation(bool BPSK_or_MSK)
{
	if (BPSK_or_MSK)
		ModulationBPSK();
	else
		ModulationMSK();
}

/** ���������� ���.
* @param *data - ������ ��� ������� ���������� ���.
* @param n - ���������� �������� � �������.
* @param is - ���� is = -1 - ������, ���� is = 1 - ��������.
*/
void CAF::fourier(cufftComplex *data, int n, int is)
{
	int i, j, istep;
	int m, mmax;
	float r,
		r1,
		theta,
		w_r,
		w_i,
		temp_r,
		temp_i;
	float pi = 3.1415926f;

	r = pi*is;   // ������ ��� ���������
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			temp_r = data[j].x;
			temp_i = data[j].y;
			data[j].x = data[i].x;
			data[j].y = data[i].y;
			data[i].x = temp_r;
			data[i].y = temp_i;
		}
		m = n >> 1;
		while (j >= m)
		{
			j -= m; m = (m + 1) / 2;
		}
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (float)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1*m;
			w_r = (float)cos((float)theta);
			w_i = (float)sin((float)theta);
			for (i = m; i < n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r*data[j].x - w_i*data[j].y;
				temp_i = w_r*data[j].y + w_i*data[j].x;
				data[j].x = data[i].x - temp_r;
				data[j].y = data[i].y - temp_i;
				data[i].x += temp_r;
				data[i].y += temp_i;
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (i = 0; i < n; i++)
		{
			data[i].x /= (float)n;
			data[i].y /= (float)n;
		}
}

/** ��������� �������� ������� ����������������.*/
//void CAF::calc_caf()
//{
//	Vec_CAF.clear();
//
//	/** ������ ������� ������������ �������.*/
//	size_t size_vec_explr_sig = Vec_ExplrSigEx.size();
//
//	/** ������ ������� �������� �������.*/
//	size_t size_vec_ref_sig = Vec_RefSigEx.size();
//
//	/** �������� size_vec_explr_sig � size_vec_ref_sig.*/
//	size_t size_diff = size_vec_explr_sig - size_vec_ref_sig;
//
//	/** ��������� ������ ��� �������.*/
//	vec_amb_func.resize(size_diff);
//	for (size_t i = 0; i < size_diff; i++)
//	{
//		vec_amb_func[i].resize(size_vec_ref_sig);
//	}
//
//	/** �������� ������ ��� ���.*/
//	std::vector<cmplx> vec_buf;
//	vec_buf.resize(size_vec_ref_sig);
//
//	double val_re, val_im;
//	double max1 = 0;
//	/** ���������� ��.*/
//	for (size_t i = 0; i < size_diff; i++)
//	{
//		val_re = 0;
//		val_im = 0;
//		std::fill(vec_buf.begin(), vec_buf.end(), cmplx{ 0, 0 });
//		for (size_t j = 0; j < size_vec_ref_sig; j++)
//		{
//			vec_buf[j].re = (Vec_RefSigEx[j].re * Vec_ExplrSigEx[j + i].re +
//				Vec_RefSigEx[j].im * Vec_ExplrSigEx[j + i].im);
//			val_re += vec_buf[j].re;
//			vec_buf[j].im = (Vec_RefSigEx[j].re * (-Vec_ExplrSigEx[j + i].im) +
//				Vec_RefSigEx[j].im * Vec_ExplrSigEx[j + i].re);
//			val_im += vec_buf[j].im;
//		}
//
//		val_re /= size_vec_ref_sig;
//		val_im /= size_vec_ref_sig;
//
//		for (size_t j = 0; j < size_vec_ref_sig; j++)
//		{
//			vec_buf[j].re -= val_re;
//			vec_buf[j].im -= val_im;
//		}
//
//		max1 = 0;
//		fourier(vec_buf.data(), int(size_vec_ref_sig), -1);
//		for (size_t k = 0; k < size_vec_ref_sig; k++)
//		{
//			vec_amb_func[i][k] = sqrt(vec_buf[k].re * vec_buf[k].re + vec_buf[k].im * vec_buf[k].im);
//			if (max1 < vec_amb_func[i][k])
//				max1 = vec_amb_func[i][k];
//		}
//		Vec_CAF.push_back(max1);
//	}
//
//	double max2 = 0;
//	for (size_t it = 0; it < Vec_CAF.size(); it++)
//	{
//		if (max2 < Vec_CAF[it])
//			max2 = Vec_CAF[it];
//	}
//	/** ���������� �������� �� � ������������� ��������� [0;1].*/
//	std::for_each(Vec_CAF.begin(), Vec_CAF.end(), [max2](double & x) { x /= max2; });
//}

/** ����������.*/
CAF::~CAF()
{}
